package com.example.passingdatabetweenactivity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.widget.Button
import android.widget.EditText

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btnApply : Button = findViewById(R.id.btnApply)
        val etName : EditText = findViewById(R.id.etName)
        val etAge : EditText = findViewById(R.id.etAge)
        val etCountry : EditText = findViewById(R.id.etCountry)

        btnApply.setOnClickListener {
            val name = etName.text.toString()
            val age = etAge.text.toString().toInt()
            val country = etCountry.text.toString()

            val person = Person(name,age,country)

            Intent(this,SecondActivity::class.java).also {
                it.putExtra("EXTRA_PERSON",person)
//                it.putExtra("EXTRA_NAME",name)
//                it.putExtra("EXTRA_AGE",age)
//                it.putExtra("EXTRA_COUNTRY",country)
                startActivity(it)
            }
        }
    }
}

